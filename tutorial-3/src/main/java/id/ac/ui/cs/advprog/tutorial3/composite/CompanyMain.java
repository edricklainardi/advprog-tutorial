package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompanyMain {
    public static void main(String[] args) {
        Company company = new Company();

        Ceo hai = new Ceo("hai", 500000.00);
        company.addEmployee(hai);

        Cto cupu = new Cto("cupu", 320000.00);
        company.addEmployee(cupu);

        BackendProgrammer xin = new BackendProgrammer("Xin", 94000.00);
        company.addEmployee(xin);

        BackendProgrammer dreams = new BackendProgrammer("Dreams", 200000.00);
        company.addEmployee(dreams);

        FrontendProgrammer imagination = new FrontendProgrammer("Imagination",66000.00);
        company.addEmployee(imagination);

        FrontendProgrammer val = new FrontendProgrammer("Val", 130000.00);
        company.addEmployee(val);

        UiUxDesigner xora = new UiUxDesigner("Xora", 177000.00);
        company.addEmployee(xora);

        NetworkExpert lun = new NetworkExpert("LuN", 83000.00);
        company.addEmployee(lun);
    }
}
